// 统一入口文件、方便引用全局资源、如css、第三方库等
import Vue from 'vue'
import './style/base.css'
Vue.config.productionTip = false
// 执行模块自身逻辑
require(`./modules/${process.env.VUE_APP_NAME}/main.js`)



