const { app1Name, publicPath, httpApi } = require('../../modulesConfig/app1.config.js')

// 默认配置，app1为共享模块
const config = {
  port: 6001,
  publicPath, // 作为共享模块，必须配置完整路径，否则无法引用额外的css、js
  moduleFederation: {
    name: app1Name, // 模块名称
    filename: `${app1Name}.js`, // 打包后的文件名称
    exposes: {
      // 对外暴露的组件
      "./app1-ui": "./src/modules/app1/components/common.js", // 暴露所有组件
      "./button": "./src/modules/app1/components/button/index.js", // 暴露单独组件，通过use()使用
      "./divider": "./src/modules/app1/components/divider/divider.vue" // 通过组件注册方式使用
    },
  },
  title: 'app1', // 页面标题
  httpApi
}

module.exports = config