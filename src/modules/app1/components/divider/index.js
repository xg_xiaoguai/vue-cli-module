import Button from './divider.vue'
export default {
  install(Vue) {
    Vue.component('PtDivider', Button)
  }
}