// 
import './style.css'
import Button from './button/index'
import Divider from './divider/index'
const install = async (vue) => {
  [
    Button,
    Divider
  ].forEach(component => {
    vue.use(component)
  })
}
export default {
  install,
  Button,
  Divider
}
