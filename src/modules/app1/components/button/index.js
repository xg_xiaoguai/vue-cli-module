import Button from './button.vue'
export default {
  install(Vue) {
    Vue.component('PtButton', Button)
  }
}