// app1/main.js
import Vue from 'vue'
import App from './App.vue'
import { parseTime } from '@/common/util'
console.log(parseTime(Date.now(), '{y}-{m}-{d} 星期{a}'))

new Vue({
  render: h => h(App),
}).$mount('#app')
