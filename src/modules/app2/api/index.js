import http from '@/common/axios.config'

export function getTableList(data) {
  return http({
    url: '/mealList',
    method: 'get',
    params: data
  })
}
