// app2/main.js
import Vue from 'vue'
import App from './App.vue'

import('app1/app1-ui').then(app1 => {
  Vue.use(app1.default)
  init()
}).catch(err => {
  alert('app1请求失败！')
  init()
})

function init() {
  new Vue({
    render: h => h(App),
  }).$mount('#app')
}
