const { app1Url } = require('../../modulesConfig/app1.config.js')
const { app2Name, publicPath, httpApi } = require('../../modulesConfig/app2.config.js')
// 默认配置，app2既是引用模块，也是共享模块
const config = {
  port: 6002,
  publicPath,
  moduleFederation: {
    name: app2Name, // 模块名称
    filename: `${app2Name}.js`,
    exposes: {
      "./home": "./src/modules/app2/views/index", // 需要相对于根目录的路径
      "./detail": "./src/modules/app2/views/detail"
    },
    remotes: {
      "app1": app1Url
    }
  },
  title: 'app2', // 页面标题
  httpApi
}

module.exports = config