// app3/main.js
import Vue from 'vue'
import App from './App.vue'
import router from './router/router'


import('app1/app1-ui').then(app1 => {
  Vue.use(app1.default)
}).catch(() => {
  alert('app1请求失败！')
}).finally(() => {
  new Vue({
    router,
    render: h => h(App),
  }).$mount('#app')
})
