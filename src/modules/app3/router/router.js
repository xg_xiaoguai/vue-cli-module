import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const router = new VueRouter({
  routes: [
    {
      path: '/',
      component: () => import('../views/index')
    },
    {
      path: '/home',
      component: () => import('app2/home')
    },
    {
      path: '/detail',
      component: () => import('app2/detail')
    }
  ]
})
export default router
