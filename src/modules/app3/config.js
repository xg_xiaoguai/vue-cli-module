const { app1Url } = require('../../modulesConfig/app1.config')
const { app2Url } = require('../../modulesConfig/app2.config')
const { httpApi, publicPath } = require('../../modulesConfig/app3.config')

// 默认配置，仅作为引用模块
const config = {
  port: 6003,
  publicPath,
  moduleFederation: {
    remotes: {
      "app1": app1Url,
      "app2": app2Url
    }
  },
  title: 'app3', // 页面标题
  httpApi
}

module.exports = config