// app3配置信息

const baseConfig = {
  development: {
    publicPath: "",
    httpApi: "http://localhost:2020/app3-dev" // 异步请求地址
  },
  test: {
    publicPath: "/app3",
    httpApi: "http://localhost:2020/app3-test" // 异步请求地址
  },
  production: {
    publicPath: "/app3",
    httpApi: "http://localhost:2020/app3-pro"
  }
}

module.exports = baseConfig[process.env.VUE_APP_ENV_NAME]
