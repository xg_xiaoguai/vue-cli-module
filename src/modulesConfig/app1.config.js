// app1配置信息，app1为共享模块
const app1Name = "app1" // 共享模块变量名
const publicPathDev = "http://localhost:6001" // 开发环境：作为共享模块，应配置完整域名，和访问地址一致，包括端口
const publicPathTest = "http://localhost:1992/app1" // 非开发环境：推荐使用完整路径，只有在引用模块和共享模块是同一个服务器下才可以使用相对路径
const publicPathPro = "http://localhost:1992/app1" // 生产

const baseConfig = {
  development: {
    app1Name,
    publicPath: publicPathDev,
    app1Url: `${app1Name}@${publicPathDev}/${app1Name}.js`, // 提供引用模块使用的当前共享模块的请求路径
    httpApi: "http://localhost:3000", // 异步请求地址
  },
  test: {
    app1Name,
    publicPath: publicPathTest,
    app1Url: `${app1Name}@${publicPathTest}/${app1Name}.js`,
    httpApi: "http://localhost:2020/app1-test",
  },
  production: {
    app1Name,
    publicPath: publicPathPro,
    app1Url: `${app1Name}@${publicPathPro}/${app1Name}.js`,
    httpApi: "http://localhost:2020/app1-pro",
  }
}

module.exports = baseConfig[process.env.VUE_APP_ENV_NAME]
