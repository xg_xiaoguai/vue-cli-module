// app2配置信息，app2既是引用模块，也是共享模块

const app2Name = "app2"
const publicPathDev = "http://localhost:6002"
const publicPathTest = "http://localhost:1992/app2"
const publicPathPro = "http://localhost:1992/app2"

const baseConfig = {
  development: {
    app2Name,
    publicPath: publicPathDev,
    app2Url: `${app2Name}@${publicPathDev}/${app2Name}.js`,
    httpApi: "http://localhost:3000" // 异步请求地址
  },
  test: {
    app2Name,
    publicPath: publicPathTest,
    app2Url: `${app2Name}@${publicPathTest}/${app2Name}.js`,
    httpApi: "http://localhost:2020/app2-test"
  },
  production: {
    app2Name,
    publicPath: publicPathPro,
    app2Url: `${app2Name}@${publicPathPro}/${app2Name}.js`,
    httpApi: "http://localhost:2020/app2-pro"
  }
}

module.exports = baseConfig[process.env.VUE_APP_ENV_NAME]
