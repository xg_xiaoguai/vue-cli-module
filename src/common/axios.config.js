import axios from "axios"

const http = axios.create({
  baseURL: process.env.VUE_APP_HTTP_URL,
  timeout: 20000
})

http.interceptors.request.use(config => {
  return config
})

http.interceptors.response.use(res => {
  console.log(res)
  return res
}, error => {
  // 响应错误处理
  return Promise.reject(error)
})

export default http