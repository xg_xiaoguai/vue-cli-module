// vue.config.js
const minimist = require('minimist')
// 获取命令行参数，得到需要运行或者打包的模块名，根据模块名获取对应配置信息
const { name, env } = minimist(process.argv.slice(2))
if (!name) throw Error('缺少参数：name')
// 重新定义环境变量，新增测试环境
process.env.VUE_APP_ENV_NAME = env || process.env.NODE_ENV
// 保存模块名，用于入口文件引用对应的模块
process.env.VUE_APP_NAME = name
// 获取模块对应配置
const { port = 6000, publicPath, moduleFederation, title = name, httpApi } = require(`./src/modules/${name}/config.js`)
// 配置异步请求地址，开发环境需要proxy，测试和生产直接使用httpApi
process.env.VUE_APP_HTTP_URL = process.env.NODE_ENV === "development" ? "/http-api" : httpApi
module.exports = {
  // 开发环境下，作为共享模块时，需要设置完整地址，否则引用模块无法获取其他资源如css
  publicPath,
  pages: {
    index: {
      entry: './src/main.js',
      title // 页面标题
    }
  },
  // 打包输出到对应模块下
  outputDir: `./src/modules/${name}/dist`,
  configureWebpack: {
    output: {
      uniqueName: `mudules-${name}`, // 设置webpack全局唯一变量，避免webpack冲突，导致热更新失败问题
    },
  },
  productionSourceMap: process.env.VUE_APP_ENV_NAME === 'production' ? false : true,
  chainWebpack: (config) => {
    /* 删除模块，避免和ModuleFederationPlugin冲突 */
    config.optimization.delete("splitChunks");
    // 模块联邦，暴露或引入共享模块
    config
      .plugin("module-federation-plugin")
      .use(require("webpack").container.ModuleFederationPlugin, [
        moduleFederation
      ]);
  },

  devServer: {
    port,
    proxy: {
      "/http-api": {
        target: httpApi,
        changeOrigin: true,
        pathRewrite: { '^/http-api': '' }
      }
    },
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
      "Access-Control-Allow-Headers":
        "X-Requested-With, content-type, Authorization",
    },
  },
};
