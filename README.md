# 项目介绍
基于vue-cli 5 的多模块微前端项目，主要使用webpack 5 的ModuleFederationPlugin插件实现模块共享和引用。

# 开发
## 运行模块
需要运行那个模块就修改参数name
```
npm run serve -- --name=app1
```

## 打包测试
```
npm run test -- --name=app1
```

## 打包生产
```
npm run build -- --name=app1
```
注意：在package.json里配置命令参数不需要中间的 -- ，如serve:app1

# 模块说明

## app1

作为通用组件共享出去，让其他模块引用，开发环境下共享模块的publicPath需要配置完整路径，否则无法请求额外的css和js
测试和生产环境下共享模块和引用模块在同一服务下可使用相对路径，否则也需要配置完整路径

1、通过配置ModuleFederationPlugin暴露模块

## app2
引用app1中的组件，以达到不必重新构建，自动获取app1中最新组件的目的

同时共享页面组件

1、通过配置ModuleFederationPlugin定义模块地址

2、引用组件库模块时，需要使用import请求模块
```
import('peento/peento-ui').then(res => {
  const peento = res.default
  Vue.use(peento)
  new Vue({
    render: h => h(App),
  }).$mount('#app')
})
```

## app3
同时引用app1通用组件和app2页面组件

